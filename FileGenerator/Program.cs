﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FileGenerator
{
    class Program
    {
        private static readonly Random Random = new Random();

        static void Main(string[] args)
        {
            long arrayLength;
            if (args.Length < 1 || !long.TryParse(args[0], out arrayLength))
            {
                Console.WriteLine("Please provide file length");
                Console.ReadLine();
                return;
            }

            Console.WriteLine("Creating file...");
            using (var fileWriter = new StreamWriter("file.txt"))
            {
                while (arrayLength > 0)
                {
                    var stringArray = CreateArrayOfStrings((int) (arrayLength>10000 ? 10000 : arrayLength));
                    foreach (var str in stringArray)
                    {
                        fileWriter.WriteLine(str);
                    }

                    arrayLength -= 10000;
                }

                fileWriter.Close();
                Console.WriteLine("Finished!");
            }

            Console.ReadLine();
        }

        public static List<string> CreateArrayOfStrings(int arrayLength)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var result = new List<string>();
            int number;
            while (arrayLength > 60)
            {     
                number = Random.Next(1, int.MaxValue);                
                var newString = $"{number}. {new string(Enumerable.Repeat(chars, Random.Next(10, 30)).Select(s => s[Random.Next(s.Length)]).ToArray())}";
                result.Add(newString);
                arrayLength -= newString.Length+2;
            }

            number = Random.Next(1, int.MaxValue);
            var lastString = new StringBuilder();
            lastString.Append($"{number}. ");            
            lastString.Append($"{new string(Enumerable.Repeat(chars, arrayLength - lastString.Length - 2).Select(s => s[Random.Next(s.Length)]).ToArray())}");
            result.Add(lastString.ToString());
            return result;
        }
    }
}
