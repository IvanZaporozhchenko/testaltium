﻿using ActionQueueSpace;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TestAltium
{
    class SortingInBunchesService
    {
        private readonly int _heapSize;
        private readonly List<Task> _tasks = new List<Task>();
        private readonly List<Task> _continueTask = new List<Task>();

        public SortingInBunchesService(int heapSize)
        {
            _heapSize = heapSize;
        }

        /// <summary>
        /// Sorting file into bunches and return blocks start positions
        /// </summary>        
        public List<long> SortFileIntoBunches(string inputFileName, string outputFileName)
        {
            var bunchPositions = new List<long>();
            long filePosition = 0;
            var inputFileLength = new FileInfo(inputFileName).Length;
            var actionQueue = new ActionQueue();

            //devide heap into 5 peaces, so 5 threads could be working in the same time
            var inputFileService = new FileService(inputFileName, _heapSize / 5);
            var outputFileService = new FileService(outputFileName, _heapSize / 5);

            bunchPositions.Add(0);
            while (filePosition < inputFileLength)
            {
                var result = inputFileService.ReadBlock(filePosition);                
                List<Item> list = result.Item1;                
                RemoveCompletedTaskFromList(_tasks);
                RemoveCompletedTaskFromList(_continueTask);
                WaitForTasksIfNeeded();

                var task = Task.Factory.StartNew(() =>
                {
                    list.Sort();
                    return list;
                });
                var continueTask = task.ContinueWith(resultTask =>
                {                    
                    var writingTask = actionQueue.AddAction(() => {
                        var position = outputFileService.WriteToFile(resultTask.Result);
                        resultTask.Result.Clear();
                        bunchPositions.Add(position);
                    });
                    lock (_tasks)
                    {
                        _tasks.Add(writingTask);
                    }
                });
                lock (_tasks)
                {
                    _tasks.Add(task);                    
                }
                lock (_continueTask)
                {
                    _continueTask.Add(continueTask);
                }
                
                filePosition = result.Item2;
            }

            lock (_tasks)
            {
                Task.WaitAll(_tasks.ToArray());
            }
            lock (_continueTask)
            {
                Task.WaitAll(_continueTask.ToArray());
            }
            lock (_tasks)
            {
                Task.WaitAll(_tasks.ToArray());
            }
            outputFileService.Close();
            inputFileService.Close();                        
            return bunchPositions;
        }

        private void WaitForTasksIfNeeded()
        {
            lock (_continueTask)
            {
                foreach (var conTask in _continueTask)
                {
                    if (conTask.Status == TaskStatus.Running || conTask.Status == TaskStatus.WaitingToRun)
                    {
                        Task.WaitAll(conTask);
                    }
                }
            }
            if (_tasks.Count >= 4)
            {
                lock (_tasks)
                {
                    Task.WaitAny(_tasks.ToArray());
                }
            }
        }

        private void RemoveCompletedTaskFromList(List<Task> tasks)
        {
            List<Task> completedTasks;
            lock (tasks)
            {
                completedTasks = tasks.Where(x => x.IsCompleted).ToList();               
            }

            foreach (var completedTask in completedTasks)
            {
                tasks.Remove(completedTask);
            }
        }
    }
}
