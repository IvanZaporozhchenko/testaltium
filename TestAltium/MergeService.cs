﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TestAltium.Infrastructure;
using Wintellect.PowerCollections;

namespace TestAltium
{
    public class MergeService
    {
        private readonly int _heapSize;
        private readonly List<Task> _readingTasks = new List<Task>();

        public MergeService(int heapSize)
        {
            _heapSize = heapSize;                        
        }

        public void MergeTempFile(List<long> fragmentPositions, string tempFileName, string outputFileName)
        {                        
            var fragments = InitListOfFragmentPositions(fragmentPositions);
            var oneBlockSize = _heapSize / (fragments.Count);

            var inputFileService = new FileService(tempFileName, oneBlockSize);
            var outputFileService = new FileService(outputFileName, oneBlockSize);

            var listOfLinkedList = new List<LinkedList<Item>>();
            var outputBufferSize = 0;        
            for (int index = 0; index < fragmentPositions.Count - 1; index++)
            {                
                listOfLinkedList.Add(new LinkedList<Item>(ReadNextSmallBlockForFragment(inputFileService, fragments, index)));
            }

            var firstElements = new OrderedBag<ItemIndex>();
            GetFirstElements(listOfLinkedList, firstElements);
            var outputBuffer = new List<Item>();
            while (true)
            {                                                
                if (!firstElements.Any())
                {
                    WriteToFile(outputFileService, outputBuffer);
                    outputFileService.Close();
                    inputFileService.Close();
                    break;
                }

                var element = firstElements.GetFirst();
                firstElements.RemoveFirst();
                outputBuffer.Add(element.Item);
                outputBufferSize += element.Item.Bytes.Length;
                if (outputBufferSize >= oneBlockSize)
                {
                    outputBufferSize = 0;                    
                    WriteToFile(outputFileService, outputBuffer);
                    Task.WaitAll(_readingTasks.ToArray());
                    _readingTasks.Add(Task.Factory.StartNew(() =>
                    {
                        ReadNewBlock(listOfLinkedList, inputFileService, fragments);
                    }));
                }
                            
                lock (listOfLinkedList[element.Index])
                {
                    listOfLinkedList[element.Index].RemoveFirst();                                     
                }               

                if (listOfLinkedList[element.Index].Count == 0)
                {
                    Task.WaitAll(_readingTasks.ToArray());                    
                    lock (listOfLinkedList[element.Index])
                    {
                        if (listOfLinkedList[element.Index].Count == 0)
                        {
                            listOfLinkedList[element.Index] = new LinkedList<Item>(ReadNextSmallBlockForFragment(inputFileService, fragments, element.Index));
                        }                                  
                    }                    
                }

                var newElement = GetNewFirstElement(listOfLinkedList, element.Index);
                if (newElement != null)
                {
                    firstElements.Add(newElement);
                }
            }            
        }

        private ItemIndex GetNewFirstElement(List<LinkedList<Item>> listOfLinkedList, int elementIndex)
        {            
            Item newElement = null;
            if (listOfLinkedList[elementIndex].Count > 0)
            {
                lock (listOfLinkedList[elementIndex])
                {
                    newElement = listOfLinkedList[elementIndex].First.Value;
                }
            }

            if (newElement != null)
            {
                return new ItemIndex
                {
                    Item = newElement,
                    Index = elementIndex
                };
            }

            return null;
        }

        private void ReadNewBlock(List<LinkedList<Item>> listOfLinkedList, FileService inputFileService, List<FragmentPosition> fragments)
        {
            var minimumLinkedListIndex = GetMinimumIndex(listOfLinkedList.Select(x => x.Count).ToArray());
            var nextValues = ReadNextSmallBlockForFragment(inputFileService, fragments, minimumLinkedListIndex);
            if (nextValues.Count == 0)
            {                                
                for (int index = 0; index < listOfLinkedList.Count; index++)
                {
                    if (index == minimumLinkedListIndex)
                    {
                        continue;
                    }

                    nextValues = ReadNextSmallBlockForFragment(inputFileService, fragments, index);
                    if (nextValues.Count != 0)
                    {
                        var linkedList = listOfLinkedList[index];
                        lock (linkedList)
                        {
                            linkedList.AddRange(nextValues);
                        }                        

                        break;
                    }
                }
            }
            else
            {
                var linkedList = listOfLinkedList[minimumLinkedListIndex];
                lock (linkedList)
                {
                    listOfLinkedList[minimumLinkedListIndex].AddRange(nextValues);
                }                
            }
        }

        private static void WriteToFile(FileService outputFileService, List<Item> outputBuffer)
        {            
            outputFileService.WriteToFile(outputBuffer);
            outputBuffer.Clear();
        }

        private static void GetFirstElements(List<LinkedList<Item>> listOfLinkedList, OrderedBag<ItemIndex> firstElements)
        {
            for (var index = 0; index < listOfLinkedList.Count; index++)
            {
                var linkedList = listOfLinkedList[index];
                var first = linkedList.First;
                if (first != null)
                {
                    firstElements.Add(new ItemIndex
                    {
                        Item = first.Value,
                        Index = index
                    });
                }
            }
        }

        private static List<FragmentPosition> InitListOfFragmentPositions(List<long> fragmentPositions)
        {            
            var fragments = new List<FragmentPosition>(fragmentPositions.Count);
            for (var index = 0; index < fragmentPositions.Count; index++)
            {                
                fragments.Add(new FragmentPosition
                {
                    Start = fragmentPositions[index],
                    Current = fragmentPositions[index]                 
                });
            }
            
            return fragments;
        }

        private List<Item> ReadNextSmallBlockForFragment(FileService inputFileService, 
            List<FragmentPosition> fragmentPositions,             
            int index)
        {
            var fragmentEnd = fragmentPositions[index + 1].Start;
            if (fragmentPositions[index].Current == fragmentEnd)
            {
                return new List<Item>();
            }

            var result = inputFileService.ReadBlock(fragmentPositions[index].Current, fragmentEnd);
            fragmentPositions[index].Current = result.Item2;
            return result.Item1;
        }

        private int GetMinimumIndex<T>(T[] firstElements) where T : IComparable<T>
        {
            T minimumValue = default(T);
            int minimumIndex = 0;
            for (int index = 0; index < firstElements.Length; index++)
            {
                if (firstElements[index] != null)
                {
                    minimumValue = firstElements[index];
                    minimumIndex = index;
                    break;
                }
            }

            for (int index = minimumIndex + 1; index < firstElements.Length; index++)
            {
                if (firstElements[index] != null)
                {
                    if (firstElements[index].CompareTo(minimumValue) < 0)
                    {
                        minimumValue = firstElements[index];
                        minimumIndex = index;
                    }
                }
            }

            return minimumIndex;
        }        

        private class FragmentPosition
        {
            public long Start { get; set; }

            public long Current { get; set; }            
        }
    }
}
