﻿using System;

namespace TestAltium
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Please provide input and output file in arguments");
                Console.ReadLine();
                return;
            }

            var externalSortService = new ExternalSortService();
            externalSortService.Sort(args[0], args[1]);
            Console.WriteLine("Finished!");
            Console.ReadLine();
        }                                         
    }    
}
