﻿using System;
using System.Diagnostics;
using System.IO;

namespace TestAltium
{
    class ExternalSortService
    {
        private const int HEAP_SIZE = 500000000;

        public void Sort(string inputFileName, string outputFileName)
        {
            var tempFileName = $"{outputFileName}.temp.txt";            
            var sortingInBunchService = new SortingInBunchesService(HEAP_SIZE);
            CreateOutputFile(tempFileName);
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var bunchPositions = sortingInBunchService.SortFileIntoBunches(inputFileName, tempFileName);
            stopWatch.Stop();
            Console.WriteLine($"1 step takes {stopWatch.Elapsed}");
            stopWatch.Restart();
            var mergeService = new MergeService(HEAP_SIZE);
            CreateOutputFile(outputFileName);
            mergeService.MergeTempFile(bunchPositions, tempFileName, outputFileName);
            stopWatch.Stop();            
            File.Delete(tempFileName);
            Console.WriteLine($"2 step takes {stopWatch.Elapsed}");
        }

        private static void CreateOutputFile(string fileName)
        {
            using (var stream = File.Create(fileName))
            {
                stream.Close();
            }
        }
    }
}
