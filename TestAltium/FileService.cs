﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TestAltium
{
    public class FileService
    {
        private readonly string _fileName;
        private readonly int _bytesInBunch;        
        private FileStream _fileStream;

        public FileService(string fileName, int bytesInBunch)
        {
            _fileName = fileName;
            _bytesInBunch = bytesInBunch;
        }

        /// <summary>
        /// Write list of Items to file
        /// </summary>        
        /// <returns>Return end of file position</returns>
        public long WriteToFile(List<Item> list)
        {
            if (_fileStream == null)
            {
                _fileStream = new FileStream(_fileName, FileMode.Open, FileAccess.ReadWrite);
            }
                                            
            WriteToStream(_fileStream, list);
            return _fileStream.Position;
        }     
        
        /// <summary>
        /// Close file
        /// </summary>
        public void Close()
        {  
            if (_fileStream != null)
            {
                _fileStream.Close();
                _fileStream.Dispose();
                _fileStream = null;
            }
        }

        /// <summary>
        /// Get items from block
        /// </summary>        
        /// <returns>List of items and where block is ending</returns>
        public Tuple<List<Item>, long> ReadBlock(long filePosition, long? filePositionMax = null)
        {
            if (_fileStream == null)
            {
                _fileStream = new FileStream(_fileName, FileMode.Open, FileAccess.Read);
            }

            _fileStream.Seek(filePosition, SeekOrigin.Begin);
            int arrayLength = (int) (filePositionMax.HasValue && (filePositionMax.Value - filePosition < _bytesInBunch)
                ? filePositionMax.Value - filePosition
                : _bytesInBunch);
            byte[] byteArray = new byte[arrayLength];
            _fileStream.Read(byteArray, 0, arrayLength);                    
            var index = GetLastIndexOfEndLine(byteArray);            
            var result = GetItemsList(byteArray, index);
            var currentFilePosition = filePosition + index + 1;                        
            return new Tuple<List<Item>, long>(result, currentFilePosition);
        }

        private List<Item> GetItemsList(byte[] byteArray, int length)
        {
            var result = new List<Item>();
            int startIndex = 0;
            int stringStartIndex = 0;
            bool isDotFinded = false;
            for (int index = 0; index < length; index++)
            {
                if (!isDotFinded && byteArray[index] == '.')
                {
                    if (byteArray[index + 1] == ' ')
                    {
                        isDotFinded = true;
                        stringStartIndex = index + 2;
                        index++;
                        continue;
                    }
                }

                if (byteArray[index] == 13)
                {
                    if (byteArray[index + 1] == 10)
                    {                        
                        byte[] arrayPart = new byte[index + 2 - startIndex];
                        Array.Copy(byteArray, startIndex, arrayPart, 0, index + 2 - startIndex);
                        result.Add(new Item
                        {
                            Bytes = arrayPart,
                            StringStartIndex = stringStartIndex - startIndex
                        });
                        index++;
                        startIndex = index + 1;
                        isDotFinded = false;
                    }
                }
            }
                 
            return result;
        }

        private int GetLastIndexOfEndLine(byte[] byteArray)
        {            
            for (int index = byteArray.Length - 1; index >= 1; index--)
            {
                if (byteArray[index] == 10)
                {
                    if (byteArray[index - 1] == 13)
                    {
                        return index;                        
                    }
                }
            }

            return 0;            
        }

        private void WriteToStream(FileStream fileStream, List<Item> listOfStringInBlock)
        {
            foreach (var item in listOfStringInBlock)
            {              
                fileStream.Write(item.Bytes, 0, item.Bytes.Length);
            }                        
        }
    }
}
