﻿using System;

namespace TestAltium
{
    public class Item : IComparable<Item>
    {    
        public byte[] Bytes { get; set; }

        public int StringStartIndex { get; set; }        

        public int CompareTo(Item other)
        {
            if (other == null) return 1;
            var currentLength = Bytes.Length - StringStartIndex;
            var otherLength = other.Bytes.Length - other.StringStartIndex;            
            var result = currentLength.CompareTo(otherLength);
            if (result == 0) // length of strings is similar
            {
                for (int index = 0; index < currentLength - 2; index++)
                {
                    result = Bytes[StringStartIndex + index].CompareTo(other.Bytes[other.StringStartIndex + index]);
                    if (result != 0)
                    {
                        return result;
                    }
                }

                // if strings are the same
                currentLength = StringStartIndex - 2;
                otherLength = other.StringStartIndex - 2;
                result = currentLength.CompareTo(otherLength);
                if (result == 0) // length of number is similar
                {
                    for (int index = 0; index <= currentLength; index++)
                    {
                        result = Bytes[index].CompareTo(other.Bytes[index]);
                        if (result != 0)
                        {
                            return result;
                        }
                    }
                }
            }
            return result;
        }      
    }
}
