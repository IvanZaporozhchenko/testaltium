using System;

namespace TestAltium
{
    class ItemIndex : IComparable<ItemIndex>
    {
        public Item Item { get; set; }

        public int Index { get; set; }

        public int CompareTo(ItemIndex other)
        {
            return Item.CompareTo(other.Item);            
        }
    }
}