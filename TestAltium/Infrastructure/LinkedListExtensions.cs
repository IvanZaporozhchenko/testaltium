﻿using System.Collections.Generic;

namespace TestAltium.Infrastructure
{
    static class LinkedListExtensions
    {
        public static void AddRange<T>(this LinkedList<T> linkedList, List<T> values)
        {
            foreach (var value in values)
            {
                linkedList.AddLast(value);
            }
        }
    }
}
